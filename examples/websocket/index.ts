import { Observable, merge, interval } from 'rxjs';
import { startWith, map, repeat, debounceTime } from 'rxjs/operators';

import * as WebSocket from '../../drivers/websocket';

const echo: (input$: Observable<WebSocket.WebSocketInput>) => Observable<WebSocket.WebSocketOutput> = (
  input$,
) => {
  return input$.pipe(
    map((input: WebSocket.WebSocketInput) => ({
      input,
      message: input.data.toString().toUpperCase(),
    })),
    debounceTime(1000),
  );
};

const broadcastRunning = interval(10000).pipe(
  map((passedIntervals) => ({ broadcast: `Server running for ${passedIntervals * 10} seconds.` }))
);

const server: WebSocket.WebSocketHandler = (input$) => {
  return merge(echo(input$), broadcastRunning);
};

WebSocket.driver(server, { port: 3000 });
