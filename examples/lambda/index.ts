import { map } from 'rxjs/operators';

import * as AwsLambda from 'molen/dist/drivers/awsLambda';

type TestInput = {
  "key3": "value3",
  "key2": "value2",
  "key1": "value1",
};

const sayHello: AwsLambda.AwsLambdaHandler<TestInput, string> = (connection$) => {
  return connection$.pipe(
    map((input) => 'Hiya :)'),
  );
}

const server: AwsLambda.AwsLambdaHandler<TestInput, string> = (input$) => sayHello(input$);

export const handler = AwsLambda.driver(server);
