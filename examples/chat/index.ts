import { resolve } from 'path';
import { merge } from 'rxjs';
import { map } from 'rxjs/operators';

import * as Http from '../../drivers/http';
import * as WebSocket from '../../drivers/websocket';

const newConnections: WebSocket.WebSocketHandler = (connection$) => {
  return WebSocket.onMessage('connect', connection$).pipe(
    map((input) => ({ message: `Welcome User ${input.id}!`, broadcast: `User ${input.id} connected` })),
  );
};

const messages: WebSocket.WebSocketHandler = (connection$) => {
  return WebSocket.onMessage(/msg\:(.*)/, connection$).pipe(
    map((input) => console.log('Message:', input) || ({ broadcast: `User ${input.id}: ${input.data.substr(4)}` })),
  );
};

const httpServer: Http.HttpHandler = (input$) => Http.serveStatic(resolve(__dirname, './public'), input$);
const websocketServer: WebSocket.WebSocketHandler = input$ => merge(
  newConnections(input$),
  messages(input$),
);

Http.driver(httpServer, { port: 3000 });
WebSocket.driver(websocketServer, { port: 3001 });
