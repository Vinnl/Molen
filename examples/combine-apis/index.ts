import { combineLatest, merge, from, Observable } from 'rxjs';
import { map, flatMap, withLatestFrom } from 'rxjs/operators';
import { RxHR } from '@akanass/rx-http-request';
import { JSDOM } from 'jsdom';

import * as Molen from '../..';
import * as Http from '../../drivers/http';

type TosdrApi = { [key: string]: { rated: false | 'A' | 'B' | 'C' | 'D' | 'F' } };

const api = (input$: Observable<Http.HttpInput>) => {
  const apiInput$ = Http.get<{ service: string }>('/:service', input$);

  const apiResponses$ = apiInput$.pipe(
    map(request => [
      RxHR.get<TosdrApi>('https://tosdr.org/api/1/all.json', { json: true })
        .pipe(map(data => extractRatingFromTosdr(data.body, request.params.service))),
      RxHR.get('http://' + request.params.service)
        .pipe(map(data => getLinkToTerms(data.body)))
    ]),
    flatMap((apiResponses$) => combineLatest(apiResponses$)),
    map(([rating, documents]) => ({ rating, documents })),
  );

  return apiResponses$.pipe(
    withLatestFrom(input$, (apiResponses, input) => ({
      input,
      output: { body: apiResponses },
    }))
  );
};

const server: Molen.App = (connection$) => {
  return {
    Http: api(connection$.Http),
  }
};

const drivers: Molen.Drivers = {
  Http: Http.makeHttpDriver({ port: 3000 })
};
Molen.molen(server, drivers);

function extractRatingFromTosdr(tosdr: TosdrApi, service: string) {
  return tosdr[`tosdr/review/${service}`].rated;
}

function getLinkToTerms(page: string) {
  const fragment = JSDOM.fragment(page);

  const links = Array.from(fragment.querySelectorAll('a'))
    .filter((element: any) => ['Voorwaarden', 'Terms', 'Terms of Service', 'Terms of Use', 'Privacy Policy', 'Privacy'].includes((element.textContent as string).trim()));
    
  return links.map((element: any) => element.getAttribute('href'));
}
