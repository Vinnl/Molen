import { combineLatest, merge, from, Observable } from 'rxjs';
import { map, withLatestFrom, flatMap } from 'rxjs/operators';
import fetch from 'node-fetch';

import * as Http from '../../drivers/http';

const api = (input$: Observable<Http.HttpInput>) => {
  const apiInput$ = Http.get('/api', input$);
  
  const urls$ = from(getUrls());

  return apiInput$.pipe(
    flatMap(() => urls$),
    map(urls => ({ body: urls })),
  );
};

const redirect = (input$: Observable<Http.HttpInput & { url?: string }>) => {
  console.log('Redirecting');
  const redirectInput$ = Http.get<{ url?: string }>('/go/:url*', input$);
  
  const urls$ = from(getUrls());

  console.log('Got URL stream');
  
  return redirectInput$.pipe(
    withLatestFrom(urls$, (input, urls) => ({ redirect: `${urls[0]}/${input.params!.url || ''}` })),
  );
};

const server: Http.HttpHandler = (connection$) => {
  return merge(api(connection$), redirect(connection$));
};

Http.driver(server, { port: 3000 });

async function getUrls() {
  const sciHubId = 'Q21980377';
  const officialWebsiteProperty = 'P856';
  const sparql = `
    SELECT ?mainUrl ?otherUrls WHERE {
      { wd:${sciHubId} wdt:${officialWebsiteProperty} ?mainUrl. }
      UNION
      { wd:${sciHubId} p:${officialWebsiteProperty} [wikibase:rank wikibase:NormalRank; ps:${officialWebsiteProperty} ?otherUrls]. }
    }
    `;

  interface WikidataResponse {
    results: { bindings: Array<{
      mainUrl?: { value: string },
      otherUrls: { value: string };
    }> };
  }

  const response = await fetch(`https://query.wikidata.org/sparql?format=json&query=${encodeURIComponent(sparql)}`);
  const data: WikidataResponse = await response.json();

  const urls = data.results.bindings.map(result => result.mainUrl ? result.mainUrl.value : result.otherUrls.value);

  return urls;
}