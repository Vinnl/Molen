import { Observable, Subject } from 'rxjs';

export type App<Input, Output> = (input: Observable<Input>) => Observable<Output>;

export type Driver<Input, Output, Options = undefined, ReturnValue = void> =
  (app: App<Input, Output>, options?: Options) => ReturnValue;

type DriverInputType<D> = D extends Driver<infer InputType, any> ? InputType : never;
type DriverOutputType<D> = D extends Driver<any, infer OutputType> ? OutputType : never;
