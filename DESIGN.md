Molen is a prototype aimed at exploring how to apply Functional and Reactive Programming to the back-end.

This document lists some of the ideas guiding its current development. They are not final; input is very much welcomed. Furthermore, the project might go nowhere after some experimentation, in which case it might just sit here waiting to be discovered by a dead-repo-archaeolist. Hi there!

# Design goals

The main goal of this project is to make the advantages of Observables and Functional and Reactive Programming shine. Ultimately, the primary use case should be where the benefits of using FRP outweigh its downsides.

## Easy async

There are a lot of tasks that involve asynchronicity: database queries, API requests, timeouts, etc. By embracing the power of the Observable, asynchronous programming is first-class.

## Prevent bugs

By eschewing side-effects and preferring [pure functions](https://en.wikipedia.org/wiki/Pure_function), we both ensure that code is easy to test (compare the output for a given input), and that it is easy to follow (the effect of code changes is local to those changes). Thus, a developer using Molen should preferably be outputting descriptions of the side-effects they want to perform, deferring the heavy lifting of executing those side effects to external libraries.

## Let types do the work

In Molen, developers write functions that each perform a small step of transforming a given input to the desired output. This style of programming is greatly aided by static types. Thus, although Molen can be used from Javascript, it strongly encourages the use of TypeScript using strict mode, and does no extra effort to compensate when developers break assumptions made explicit through static typing. In other words: if a function parameter in Molen is of type `string`, it will not do a check whether the developer passed in `undefined` - after all, TypeScript could have told them not to do that.

## Experiment rapidly, but safely

Molen is an experiment with ideas that are mostly new to me and, as far as I can see, the Javascript community. Being able to iterate rapidly is therefore crucial, in order to judge whether an idea is good or not in as early a stage as possible.

It is built on the idea that rapid experimentation is most likely to happen when it is safe to do so. Adherance to semantic versioning, automated unstable releases and automated testing all contribute to safe experimentation, and are therefore important tenets of Molen development.

# Accepted trade-offs

## Learning curve

An important disadvantage of Observables and FRP is that the learning-curve is quite high, at least in a world in which most developers have not learned them as their first paradigm (also know as the Real World). This is a shame, and while it's important to recognise and cater for these difficulties, the use of FRP is the raison d'être of this library and thus, that will not change.

## Compatibility with Express middleware

Although it would be great to support all existing Express/Koa middleware, given that they are free to perform side-effects any moment they like, I currently do not see a way to make this work while respecting Molen's design goals. If someone manages to make it work, a Merge Request would be great; but if not, the design goals are more important.

# Non-goals

- Molen is not meant to promote a specific FRP library. At the time of writing, it uses [RxJS](http://reactivex.io/rxjs) because it appears to be most widely-used and fits the project goals well.

# Acknowledgements

- [Redux](https://redux.js.org/), the first project showing me the true power of pure functions.
- [Cycle.js](https://cycle.js.org/), which demonstrated how a well-designed framework can help using and understanding the benefits of Functional and Reactive Programming.
- [Warp](https://seanmonstar.com/post/176530511587/warp), which showed how types and functional operators can combine to form a powerful pipeline transforming back-end input into output.