Molen is still very much being prototyped, so any input on that is welcome. The easiest way is probably by [creating an issue](https://gitlab.com/Vinnl/Molen/issues/new) in which you share your input. But if you e.g. have feedback on the [design principles](https://gitlab.com/Vinnl/Molen/blob/master/DESIGN.md) or simply want to contribute code, feel free to [submit a merge request](https://gitlab.com/Vinnl/Molen/merge_requests/new) and we can discuss it.

# Releasing a new version

Obviously only relevant to those with sufficient permissions:

- Update CHANGELOG.md, moving the items below the `Unreleased` heading below your new version
- Run `yarn version` (`--major`, `--minor` or `--patch`)
- Push the `master` branch with this new version to have a snapshot build published (as `@next`)
- `git push --tags` to have the actual new version published
