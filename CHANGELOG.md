# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2018-10-09

- Add deprecation notice.

## [0.1.2] - 2018-09-18

### Bugfixes

- Remove irrelevant example code from README.

## [0.1.1] - 2018-09-18

### Bugfixes

- Links in the README that also work on non-GitLab pages.
- More informative README.

## [0.1.0] - 2018-09-13

### Bugfixes

### New features

- First alphe release open for feedback!
