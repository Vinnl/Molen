Molen is a prototype aimed at exploring how to apply Functional and Reactive Programming to the back-end.

Please take note that this is still highly experimental, and the API is unlikely to be perfect (and hence likely to change).

# DEPRECATION NOTICE

The experiment has ended; the code is primarily preserved for posterity. My primary conclusion would be that FRP is not especially better-suited to regular web server use cases than, say, Express. The main reason for this is that advanced async operations do not play a large role in such use cases, and since the ecosystem is not designed around Observables, the marshalling of data into and out of Observables, combined with the added indirection, is not worth the benefits.

If you are interested in using the npm package name `molen`, contact molen+npm@VincentTunru.com.

# Why Molen

Molen is built around the idea that asynchronicity plays a large role in many back-ends, and that many common operations can benefit from the reactive paradigm. For example, reactive operators can make it trivial to [limit the amount of requests](https://rxjs-dev.firebaseapp.com/api/operators/debounceTime), [time-out operations that take too long](https://rxjs-dev.firebaseapp.com/api/operators/race), [retry failed actions](https://rxjs-dev.firebaseapp.com/api/operators/retry), [throttle requests](https://rxjs-dev.firebaseapp.com/api/operators/throttleTime), etc.

Furthermore, Molen aims to minimise the programmer's cognitive load and thus reduce the number of bugs. The two main tools to achieve this are by [encouraging programming without side effects](https://vincenttunru.com/The-case-for-Functional-Reactive-Programming/), and by [being written in TypeScript](https://staltz.com/all-js-libraries-should-be-authored-in-typescript.html).

For more information, take a look at [the design principles](https://gitlab.com/Vinnl/Molen/blob/master/DESIGN.md).

# Getting started

Install Molen:

    npm install molen

Unfortunately there is no extensive documentation on how to get started yet, because the API is not yet final. However, do check out [the example project](https://github.com/Vinnl/molen-example-event-sourcing) for basic usage.

# Examples

For more examples, see [the example project](https://github.com/Vinnl/molen-example-event-sourcing) and [the examples/ directory](https://gitlab.com/Vinnl/Molen/tree/master/examples).

## A simple HTTP server

```typescript
import * as Http from 'molen/drivers/http';
import { map } from 'rxjs/operators';

const app: Http.HttpHandler = (input$) => {
  return input$.pipe(
    map(() => ({ body: 'Hello, world!' })),
  );
};

Http.driver(app, { port: 3000 });
```

## An AWS Lambda function

```typescript
import * as AwsLambda from 'molen/drivers/awsLambda';
import { map } from 'rxjs/operators';

type Input = {};

const lambdaFunction: AwsLambda.AwsLambdaHandler<Input, string> = (input$) => {
  return input$.pipe(
    map((input) => 'Hello, world!'),
  );
}

export const handler = AwsLambda.driver(lambdaFunction);
```

## A WebSocket server

```typescript
import * as WebSocket from 'molen/drivers/websocket';
import { map } from 'rxjs/operators';

const echoApp: WebSocket.WebSocketHandler = (input$) => {
  return input$.pipe(
    map((input) => `Hello, you said: ${input.data.toString().toUpperCase()}`),
  );
}

WebSocket.driver(echoApp, { port: 3000 });
```

# Recent changes

See the [Changelog](https://gitlab.com/Vinnl/Molen/blob/master/CHANGELOG.md).

# Contributing

Submit ideas through [the issue tracker](https://gitlab.com/Vinnl/Molen/issues/new).

See [CONTRIBUTING.md](https://gitlab.com/Vinnl/Molen/blob/master/CONTRIBUTING.md) for more info.

# Licence

Molen is licensed under the [MIT license](https://opensource.org/licenses/MIT).
