import express, { Request as ExpressRequest, Response as ExpressResponse, RequestHandler } from 'express';
import pathToRegexp from 'path-to-regexp';
import { filter, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { Server, createServer } from 'http';

import { Driver, App } from "../index";

type Request = ExpressRequest;
type Body = string | Buffer | Array<any>;
interface Response {
  body?: Body;
  redirect?: string | { url: string; status: number; };
  /**
   * This is a workaround to be able to use Express middleware. Be aware of the downsides when using!
   *
   * Express middleware executes side-effects. That means it does not mesh well with the design of
   * Molen. Therefore take into account that there is a good chance that it will be eliminated in
   * the future, and that it does not combine well with other Observables.
   */
  unidiomaticMiddleware?: RequestHandler;
}

interface Options {
  hostname?: string;
  port?: number;
}
export type HttpInput = Request;
export type HttpOutput = Response;
export type HttpDriver = Driver<HttpInput, HttpOutput, Options, Server>;
export type HttpHandler = App<HttpInput, HttpOutput>;

const defaultOptions: Options = {
  port: 8080,
};

export const driver: HttpDriver = (app, options = defaultOptions) => {
  const expressApp = express();
  
  expressApp.use((req, res) => {
    const input$ = new Subject<HttpInput>();

    const output$ = app(input$);

    output$.subscribe(response => {
      console.log('Got response:', response);

      if (typeof response.redirect === 'string') {
        return res.redirect(response.redirect);
      }
      if (response.redirect) {
        return res.redirect(response.redirect.status, response.redirect.url);
      }

      if (typeof response.body === 'string' || response.body instanceof Buffer) {
        res.send(response.body);
      }

      if (typeof response.body === 'object' || Array.isArray(response.body)) {
        res.json(response.body);
      }

      if (typeof response.unidiomaticMiddleware !== 'undefined') {
        response.unidiomaticMiddleware(req, res, () => console.log('Called next()'));
      }
      // TODO: Send error

    });

    input$.next(req);
  });

  const server = createServer(expressApp);

  server.listen(options.port, () => {
    console.log('Server listening at', options.port);
  });

  return server;
}

type UrlPart = string | undefined;

export function get<P extends { [param: string]: UrlPart } = {}>(
  path: string,
  input$: Observable<HttpInput>
): Observable<HttpInput & { params: P }> {
  return all<P>('get', path, input$);
}

export function all<P extends { [param: string]: UrlPart } = {}>(
  method: 'get' | 'post',
  path: string,
  input$: Observable<HttpInput>,
): Observable<HttpInput & { params: P }> {
  let keys: pathToRegexp.Key[] = [];
  const re = pathToRegexp(path, keys);

  const matcher: (connection: HttpInput) => boolean = (connection) => {
    console.log('Testing route:', connection.path, re, re.test(connection.path));
    
    return connection.method.toLowerCase() === method && re.test(connection.path);
  }

  return input$.pipe(
    filter(matcher),
    map((input: HttpInput) => {
      const matches = re.exec(input.path);

      input.params = keys.reduce(
        (soFar, key, index) => {
          // We know that the regex matches, because we used `matcher` above - hence the `!`.
          // Note also that the first element in `matches` is the full string, hence the `+ 1`.
          soFar[key.name] = matches![index + 1];

          return soFar;
        },
        // We assume that the type parameter passed by the user actually matches the route parameters:
        {} as P,
      );

      return input;
    }),
  );
}

export function serveStatic(
  root: string,
  input$: Observable<HttpInput>,
): Observable<HttpOutput> {
  const serveStaticMiddleware = express.static(root);

  return input$.pipe(
    map((input) => ({ unidiomaticMiddleware: serveStaticMiddleware })));
}
