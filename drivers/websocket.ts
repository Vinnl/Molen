import WebSocket from 'ws';
import { Subject, Observable } from 'rxjs';
import * as http from 'http';
import * as https from 'https';

import { Driver, App } from "../index";
import { filter } from 'rxjs/operators';

export type WebSocketInput = { data: WebSocket.Data, id: number };
export interface WebSocketOutput {
  announce?: WebSocket.Data;
  broadcast?: WebSocket.Data;
  message?: WebSocket.Data;
}

interface Options {
  hostname?: string;
  port?: number;
  server?: http.Server | https.Server;
}
export type WebSocketDriver = Driver<WebSocketInput, WebSocketOutput, Options, WebSocket.Server>;
export type WebSocketHandler = App<WebSocketInput, WebSocketOutput>;

const defaultOptions: Options = {
  port: 8080,
};

export const driver: WebSocketDriver = (app: WebSocketHandler, options = defaultOptions) => {
  const server = new WebSocket.Server({
    host: options.hostname,
    port: options.port,
    server: options.server,
  }, () => console.log('WebSocket server running on port:', options.port));

  const sockets: WebSocket[] = [];

  server.on('connection', (socket) => {
    const connectionId = sockets.push(socket);

    const input$ = new Subject<WebSocketInput>();

    const output$ = app(input$);

    socket.on('message', (message) => {
      const input: WebSocketInput = { data: message, id: connectionId };
      input$.next(input);
    });

    output$.subscribe(response => {
      if(response.broadcast) {
        sockets.forEach((otherSocket) => {
          if(socket !== otherSocket && otherSocket.readyState === WebSocket.OPEN) {
            otherSocket.send(response.broadcast);
          }
        });
      }

      if(response.announce) {
        sockets.forEach((connection) => {
          if(connection.readyState === WebSocket.OPEN) {
            connection.send(response.announce);
          }
        });
      }

      if(socket && response.message) {
        socket.send(response.message);
      }
      // TODO: Send error
    });
  });

  return server;
};

type StringMessage = WebSocketInput & { id: number; data: string };
export const onMessage: (message: string | RegExp, input$: Observable<WebSocketInput>) => Observable<StringMessage> = (message, input$) => {
  return input$.pipe(
    filter(input => (
      (typeof message === 'string' && typeof input.data === 'string' && input.data === message) ||
      (message instanceof RegExp && typeof input.data === 'string' && message.test(input.data))
    )),
  ) as Observable<StringMessage>;
}
