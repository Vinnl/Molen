import { Handler as OriginalHandler, Callback, Context } from 'aws-lambda';
import { Subject, Observable } from 'rxjs';

import { Driver, App } from "../index";

export type AwsLambdaInput<TEvent> = {
  context: Context,
  event: TEvent,
};
export type AwsLambdaOutput<TResult> = TResult;
export type AwsLambdaDriver<TEvent, TResult> = Driver<AwsLambdaInput<TEvent>, AwsLambdaOutput<TResult>, undefined, OriginalHandler>;
export type AwsLambdaHandler<TEvent, TResult> = App<AwsLambdaInput<TEvent>, AwsLambdaOutput<TResult>>;

export function driver<TEvent, TResult>(app: AwsLambdaHandler<TEvent, TResult>) {
  const connection$ = new Subject<AwsLambdaInput<any>>();
  let originalCallback: Callback;

  const output$ = app(connection$);

  const awsHandler: OriginalHandler = (event, context, callback) => {
    originalCallback = callback;

    connection$.next({ context, event });
  }

  output$.subscribe(output => {
    console.log('Got response:', output);

    if (!originalCallback) {
      // TODO Handle error
      console.error('Could not find connection');

      return;
    }

    if(output) {
      originalCallback(null, output);
    }
    // TODO: Send error
  });

  return awsHandler;
}
